<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 04.01.22 20:59:54
 */

declare(strict_types = 1);
namespace dicr\google;

use ArrayAccess;
use Google\Client;
use Google\Service\Sheets;
use RuntimeException;
use Traversable;
use Yii;
use yii\base\Arrayable;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\db\Query;
use yii\di\Instance;
use yii\web\Response;
use yii\web\ResponseFormatterInterface;

use function count;
use function is_array;
use function is_iterable;
use function is_object;

/**
 * Загружает данные в таблицы Google SpreadSheets и переадресовывает на адрес таблицы.
 *
 * Для работы необходимо задать документ $spreadSheet, либо сервис $service,
 * либо клиент $client.
 *
 * Для создания шапки таблицы а также для выбора порядка и названий
 * выгружаемых колонок данных, можно установить ассоциативный массив $fields,
 * в котором ключи - названия полей, значения - заголовки колонок.
 *
 * @property Client $client
 * @property Sheets $service
 * @property Sheets\Spreadsheet $document
 */
class SheetsResponseFormatter extends Component implements ResponseFormatterInterface
{
    /** @var int кол-во строк данных в одном запросе */
    public const ROWS_PER_REQUEST_DEFAULT = 1000;

    /** @var string название файла документа таблицы на диске */
    public string $name;

    /** @var Client|string|null авторизованный клиент Google Api */
    private Client|string|null $_client = null;

    /** @var Sheets|null сервис SpreadSheets */
    private ?Sheets $_service = null;

    /** @var Sheets\Spreadsheet|null документ SpreadSheet */
    private ?Sheets\Spreadsheet $_document = null;

    /** @var int идентификатор листа таблицы в документе */
    public int $sheetId = 1;

    /** @var array конфиг для \Google_Service_Sheets_CellFormat */
    public array $cellFormatConfig = [
        'wrapStrategy' => 'WRAP',
        'hyperlinkDisplayType' => 'LINKED'
    ];

    /** @var ?array ассоциативный массив колонок данных и их названий [field => Title] */
    public ?array $fields = null;

    /**
     * @var int кол-во строк таблицы отправляемых в одном запросе.
     * С одной стороны, есть лимит памяти на буферизацию данных,
     * с другой стороны лимит количества запросов в секунду Google.
     */
    public int $rowsPerRequest = self::ROWS_PER_REQUEST_DEFAULT;

    /** @var Sheets\RowData[] буфер срок данных для вывода */
    private array $_rows = [];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        // название документа таблицы
        if (! isset($this->name)) {
            throw new InvalidConfigException('name');
        }

        // кол-во строк в одном запросе
        if ($this->rowsPerRequest < 1) {
            throw new InvalidConfigException('rowsPerRequest');
        }

        if ($this->sheetId < 0) {
            throw new InvalidConfigException('sheetId');
        }

        if (isset($this->_client)) {
            $this->_client = Instance::ensure($this->_client, Client::class);
        } elseif (isset($this->_service)) {
            $this->_client = $this->_service->getClient();
        } elseif (! isset($this->_document)) {
            throw new InvalidConfigException('Необходимо установить spreadSheets или service или client');
        }
    }

    /**
     * Возвращает Google Client.
     *
     * @return Client|null
     */
    public function getClient(): ?Client
    {
        return $this->_client;
    }

    /**
     * Устанавливает Google клиент.
     *
     * @param Client|null $client
     * @return $this
     */
    public function setClient(?Client $client): self
    {
        $this->_client = $client;

        return $this;
    }

    /**
     * Возвращает сервис SpreadSheets.
     *
     * @return ?Sheets
     */
    public function getService(): ?Sheets
    {
        if (! isset($this->_service) && isset($this->_client)) {
            $this->_service = new Sheets($this->_client);
        }

        return $this->_service;
    }

    /**
     * Устанавливает сервис SpreadSheets.
     *
     * @param Sheets|null $service
     * @return $this
     */
    public function setService(?Sheets $service): self
    {
        $this->_service = $service;

        if ($service !== null) {
            $this->_client = $this->_service->getClient();
        }

        return $this;
    }

    /**
     * Возвращает документ.
     *
     * @return ?Sheets\Spreadsheet
     */
    public function getDocument(): ?Sheets\Spreadsheet
    {
        if ($this->_document === null) {
            $service = $this->getService();

            if ($service !== null) {
                // создаем по-умолчанию
                $this->_document = $service->spreadsheets->create(
                    new Sheets\Spreadsheet([
                        'properties' => new Sheets\SpreadsheetProperties([
                            'title' => $this->name,
                            'locale' => Yii::$app->language,
                            'timeZone' => Yii::$app->timeZone,
                            'defaultFormat' => new Sheets\CellFormat($this->cellFormatConfig),
                        ]),
                        'sheets' => [
                            new Sheets\Sheet([
                                'properties' => new Sheets\SheetProperties([
                                    'sheetId' => $this->sheetId
                                ])
                            ])
                        ]
                    ])
                );
            }
        }

        return $this->_document;
    }

    /**
     * Устанавливает документ.
     *
     * @param Sheets\Spreadsheet $document
     * @return $this
     */
    public function setDocument(Sheets\Spreadsheet $document): self
    {
        $this->_document = $document;

        return $this;
    }

    /**
     * Конвертирует входящие данные в Traversable
     *
     * @param object|array|null $data
     * @return array|Traversable
     * @throws Exception
     */
    protected static function convertData(object|array|null $data): object|array
    {
        if (empty($data)) {
            return [];
        }

        if (is_iterable($data)) {
            return $data;
        }

        if ($data instanceof Arrayable) {
            return $data->toArray();
        }

        if ($data instanceof Query) {
            return $data->each();
        }

        if ($data instanceof DataProviderInterface) {
            return $data->getModels();
        }

        if (is_object($data)) {
            return (array)$data;
        }

        throw new Exception('Неизвестный тип в response->data');
    }

    /**
     * Конвертирует строку входящих данных в массив значений.
     *
     * @param object|array|null $row - данные строки
     * @return object|array массив значений
     * @throws Exception
     */
    protected function convertRow(object|array|null $row): object|array
    {
        if (empty($row)) {
            return [];
        }

        if ($row instanceof Model) {
            return $row->attributes;
        }

        if (is_iterable($row) || ($row instanceof ArrayAccess)) {
            return $row;
        }

        if ($row instanceof Arrayable) {
            return $row->toArray();
        }

        if (is_object($row)) {
            return (array)$row;
        }

        throw new Exception('unknown row format');
    }

    /**
     * Создает ячейку таблицы.
     *
     * @param string $data
     * @return Sheets\CellData
     */
    protected function createCell(string $data): Sheets\CellData
    {
        return new Sheets\CellData([
            'userEnteredValue' => new Sheets\ExtendedValue([
                'stringValue' => $data
            ])
        ]);
    }

    /**
     * Создает строку таблицы.
     *
     * @param $data
     * @return Sheets\RowData
     * @throws Exception
     */
    protected function createRow($data): Sheets\RowData
    {
        $data = $this->convertRow($data);
        $cells = [];

        if (! empty($this->fields)) { // если заданы заголовки, то выбираем только заданные поля в заданной последовательности
            // проверяем доступность прямой выборки индекса из массива
            if (! is_array($data) && ! ($data instanceof ArrayAccess)) {
                throw new Exception('для использования списка полей fields необходимо чтобы элемент данных был либо array, либо типа ArrayAccess');
            }

            foreach (array_keys($this->fields) as $field) {
                $cells[] = $this->createCell((string)($data[$field] ?? ''));
            }
        } else { // обходим все поля
            // проверяем что данные доступны для обхода
            if (! is_array($data) && ! ($data instanceof Traversable)) {
                throw new Exception('элемент данных должен быть либо array, либо типа Traversable');
            }

            // обходим тип Traversable
            foreach ($data as $val) {
                $cells[] = $this->createCell((string)$val);
            }
        }

        return new Sheets\RowData([
            'values' => $cells
        ]);
    }

    /**
     * Отправляет строку таблицы.
     *
     * Строка добавляется в буфер и при достижении размера буфера [[rowsPerRequest]],
     * либо если $row == null отправляется запрос на добавление срок в таблицу.
     *
     * @param Sheets\RowData|null $row строка таблицы или null для отправки остатка буфера
     */
    protected function sendRow(Sheets\RowData $row = null): void
    {
        $document = $this->getDocument();
        if ($document === null) {
            throw new RuntimeException('document не установлен');
        }

        // инициализация массива
        if (empty($this->_rows)) {
            $this->_rows = [];
        }

        // добавляем строку в буфер
        if (isset($row)) {
            $this->_rows[] = $row;
        }

        // отправка запросов при переполнении буфера или сбросе
        if (! empty($this->_rows) && (! isset($row) || count($this->_rows) >= $this->rowsPerRequest)) {
            // создаем запрос на добавление строк в таблицу
            $batchUpdateRequest = new Sheets\BatchUpdateSpreadsheetRequest([
                'requests' => [
                    new Sheets\Request([
                        'appendCells' => new Sheets\AppendCellsRequest([
                            'sheetId' => $this->sheetId,
                            'rows' => $this->_rows,
                            'fields' => '*'
                        ])
                    ])
                ]
            ]);

            // отправляем запрос
            $this->_service->spreadsheets->batchUpdate(
                $document->spreadsheetId, $batchUpdateRequest
            );

            // очищаем буфер строк
            $this->_rows = [];
        }
    }

    /**
     * {@inheritDoc}
     *
     * Форматирует данные из $response->data путем выгрузки таблицы в Google SpreadSheets и редиректа на ее адрес.
     *
     * Данные в $response->data должны иметь тип:
     * array|object|\Traversable|Arrayable|Query|DataProviderInterface
     *
     * Данные в каждой строке $response->data должны иметь тип:
     * array|object|\Traversable|\ArrayAccess|Arrayable|Model
     *
     * @throws Exception
     */
    public function format($response): Response
    {
        $document = $this->getDocument();
        if ($document === null) {
            throw new RuntimeException('document не установлен');
        }

        // конвертируем входные данные
        $data = self::convertData($response->data);

        // очищаем данные ответа
        $response->data = null;

        // отправляем строку заголовка
        if (! empty($this->fields)) {
            $this->sendRow($this->createRow($this->fields));
        }

        // отправляем данные в таблицы
        foreach ($data as $rowData) {
            $this->sendRow($this->createRow($rowData));
        }

        // отправляем остатки строк в буфере
        $this->sendRow();

        // возвращаем редирект на адрес созданного документа
        return $response->redirect($document->spreadsheetUrl, 303);
    }
}
